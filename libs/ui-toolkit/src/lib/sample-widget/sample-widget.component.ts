import { Component, OnInit, ChangeDetectionStrategy, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'sgs-sample-widget',
  templateUrl: './sample-widget.component.html',
  styleUrls: ['./sample-widget.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SampleWidgetComponent implements OnInit {
  @Output() selected = new EventEmitter<any>();

  onClick() {
    this.selected.emit({ type: 'SELECTED', payload: Date.now() })
  }

  @Input() text = ''
  @Input() active = false
  @Input() counter = 0

  constructor() { }

  ngOnInit(): void {
  }

}
