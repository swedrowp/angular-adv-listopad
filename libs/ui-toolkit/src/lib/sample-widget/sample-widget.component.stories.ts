
import { SampleWidgetComponent } from './sample-widget.component';
import { boolean, number, text, withKnobs } from '@storybook/addon-knobs';
import {  action } from '@storybook/addon-actions';

export default {
  title: 'Sample Widget Component',
  decorators: [withKnobs],
}

export const primary = () => ({
  moduleMetadata: {
    imports: []
  },
  component: SampleWidgetComponent,
  props: {
    selected: action('Select clicked',{}),
    text: text('text', 'Sample text', 'groupA'),
    active: boolean('active', false, 'groupB'),
    counter: number('counter', 0, { min: 0, max: 100, step: 5 }, 'groupB'),
  }
})
export const Awesome = () => ({
  moduleMetadata: {
    imports: []
  },
  component: SampleWidgetComponent,
  props: {
    text: 'Awesome text'
  }
})