import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SampleWidgetComponent } from './sample-widget.component';



@NgModule({
  declarations: [SampleWidgetComponent],
  imports: [
    CommonModule
  ],
  exports: [SampleWidgetComponent]
})
export class SampleWidgetModule { }
