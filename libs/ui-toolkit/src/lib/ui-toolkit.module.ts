import { NgModule } from '@angular/core';
import { SampleWidgetModule } from './sample-widget/sample-widget.module';

@NgModule({
  imports: [SampleWidgetModule],
  exports: [SampleWidgetModule]
})
export class UiToolkitModule { }
