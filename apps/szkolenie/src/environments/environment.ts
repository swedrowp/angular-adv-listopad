// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { AuthConfig } from 'angular-oauth2-oidc';

export const environment = {
  production: false,


  authCodeFlowConfig: {
    // Url of the Identity Provider
    // issuer: 'https://accounts.spotify.com/authorize',
    loginUrl: 'https://accounts.spotify.com/authorize',
    requireHttps: false,
    options: {

    },
    // requestAccessToken: true,
    oidc: false,
    // sessionChecksEnabled: true,
    // sessionCheckIntervall: 20000,
    // sessionCheckIFrameUrl:  window.location.origin + '/assets/silent-refresh.html',

    // Spotify: Force window
    customQueryParams: {
      // show_dialog: 'true' // ask user permission every time
    },
    // Silent refresh implicit token in iframe
    useSilentRefresh: true,
    // silentRefreshShowIFrame: true,
    // timeoutFactor: 0.001,

    // URL of the SPA to redirect the user after silent refresh
    silentRefreshRedirectUri: window.location.origin + '/assets/silent-refresh.html',

    // URL of the SPA to redirect the user to after login
    // redirectUri: window.location.origin + '/assets/silent-refresh.html',
    redirectUri: window.location.origin + '/index.html',

    // The SPA's id. The SPA is registerd with this id at the auth-server
    // clientId: 'server.code',
    clientId: 'a81097223046407686172037bb1f6723',

    // Just needed if your auth server demands a secret. In general, this
    // is a sign that the auth server is not configured with SPAs in mind
    // and it might not enforce further best practices vital for security
    // such applications.
    // dummyClientSecret: 'secret',

    // responseType: 'code',
    responseType: 'token',

    // set the scope for the permissions the client should request
    // The first four are defined by OIDC.
    // Important: Request offline_access to get a refresh token
    // The api scope is a usecase specific one
    // scope: 'openid profile email offline_access api',
    scope: 'user-read-private playlist-modify-public',

    showDebugInformation: true,
  } as AuthConfig
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
