import { ChangeDetectionStrategy } from '@angular/core';
import { Component, Input, OnInit } from '@angular/core';
import { Album } from '../../../core/model/Album'

@Component({
  selector: 'sgs-album-card',
  templateUrl: './album-card.component.html',
  styleUrls: ['./album-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AlbumCardComponent implements OnInit {

  @Input() album!:Album

  constructor() { }

  ngOnInit(): void {
  }

}
