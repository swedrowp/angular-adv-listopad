import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ConnectableObservable, Observable, ReplaySubject, Subject, Subscription } from 'rxjs';
import { multicast, publish, publishReplay, refCount, shareReplay, takeUntil, tap } from 'rxjs/operators';
import { Album } from '../core/model/Album';
import { AlbumSearchService } from '../core/services/album-search/album-search.service'

@Component({
  selector: 'sgs-music-search',
  templateUrl: './music-search.component.html',
  styleUrls: ['./music-search.component.scss']
})
export class MusicSearchComponent implements OnInit {
  hide = false
  // albumsChanges = this.searchService.resultsChanges
  queryChanges = this.searchService.queryChanges
  constructor(private searchService: AlbumSearchService) { }

  ngOnInit(): void { }

  searchAlbums(query: string) {
    // Multicast
    this.searchService.searchAlbums(query)
  }

  albumsChanges!:  Observable<Album[]>


}
