import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from '../../../core/model/Playlist'

@Component({
  selector: 'sgs-playlists-details',
  templateUrl: './playlists-details.component.html',
  styleUrls: ['./playlists-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlaylistsDetailsComponent implements OnInit {

  @Input() playlist!: Playlist
  @Output() onEdit = new EventEmitter();

  counter = 0;

  constructor(cdr: ChangeDetectorRef) {
    // cdr.detach()

    setInterval(() => {
      this.counter++
      // cdr.detectChanges()
      // cdr.markForCheck()
    }, 1500)
  }

  ngOnInit(): void {
  }

}
