import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { selectPlaylist } from '../../actions/playlist.actions';
import { Playlist } from '../../core/model/Playlist';
import { AppState } from '../../reducers';
import { selectPlaylistList, selectPlaylistMode, selectPlaylistSelected } from '../../selectors/playlists.selectors'
import * as playlistAction from '../../actions/playlist.actions'
import { take } from 'rxjs/operators';
@Component({
  selector: 'sgs-playlists-ngrx',
  templateUrl: './playlists-ngrx.component.html',
  styleUrls: ['./playlists-ngrx.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlaylistsNgrxComponent implements OnInit {

  playlists = this.store.select(selectPlaylistList);
  selected = this.store.select(selectPlaylistSelected);
  mode = this.store.select(selectPlaylistMode);

  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
    this.store.dispatch(playlistAction.loadPlaylists())
  }


  selectById(id: Playlist['id']) {
    this.store.dispatch(playlistAction.selectPlaylist({ id }))
  }

  switchToEdit() {
    this.selected.pipe(take(1)).subscribe((playlist) => {
      if (playlist) {
        this.store.dispatch(playlistAction.editPlaylistMode({
          id: playlist.id
        }))
      }
    })
  }
  savePlaylist() {

  }

  switchToDetails(id: Playlist['id']) {
    this.store.dispatch(playlistAction.editPlaylistMode({ id }))
  }
}
