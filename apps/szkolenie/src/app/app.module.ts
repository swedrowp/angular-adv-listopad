import { BrowserModule } from '@angular/platform-browser';
import { ApplicationRef, DoBootstrap, NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { CoreModule } from './core/core.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared.module';
import { DummyComponent } from './shared/dummy/dummy.component';
import { StoreModule } from '@ngrx/store';
import { environment } from '../environments/environment';
import * as fromPlaylists from './reducers/playlists/playlists.reducer';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { PlaylistEffects } from './effects/playlist.effects';


export const routes: Routes = [
  {
    path: '', redirectTo: 'playlists', pathMatch: 'full'
  },
  {
    path: 'playlists',
    loadChildren: () => import('./playlists/playlists.module').then(m => m.PlaylistsModule)
  },
  {
    path: 'music-search',
    loadChildren: () => import('./music-search/music-search.module').then(m => m.MusicSearchModule)
  }
  ,
  {
    path: 'index.html', component: DummyComponent
  },
  {
    path: '**', redirectTo: 'playlists', pathMatch: 'full'
  }
]

@NgModule({
  declarations: [AppComponent],
  imports: [
    // SuperHiperBestDatePickerModule
    BrowserModule,
    RouterModule.forRoot(routes),
    CoreModule,
    BrowserAnimationsModule,
    SharedModule,
    StoreModule.forRoot({
      [fromPlaylists.playlistsFeatureKey]: fromPlaylists.reducer
    }, {
      runtimeChecks: environment.production ? {} : {
        strictStateImmutability: true,
        strictStateSerializability: true
      }
    }),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),
    EffectsModule.forRoot([PlaylistEffects]),
  ],
  providers: [
    // {provide:'SuperHiperBestDatePickerDateFormat', useValue:''},
    // {
    //   provide: 'PLAYLISTS_MOCK',
    //   useValue: []
    // }
  ],
  // bootstrap: [/* HeaderComponent, FooterComponent, */AppComponent],
})
export class AppModule implements DoBootstrap {
  constructor() { }

  ngDoBootstrap(appRef: ApplicationRef): void {
    // downloadConfig().then(()=>{
    const cmp = appRef.bootstrap(AppComponent, 'sgs-root')
    // cmp.instance.sidebarOpen  = true
    // })
  }
}
