import { Action, createReducer, on } from '@ngrx/store';
import { Playlist } from '../../core/model/Playlist';
import * as playlistAction from '../../actions/playlist.actions'
import { playlistsMockData } from '../../core/data/playlists.mock';


export const playlistsFeatureKey = 'playlists';

export interface PlaylistsState {
  items: Playlist[],
  selectedId: Playlist['id'] | null
  mode: string //'details' | 'edit',
  loading: boolean,
  message: string
}

export const initialState: PlaylistsState = {
  items: playlistsMockData, // :[],
  selectedId: null,
  mode: 'details',
  loading: false,
  message: ''
}


export const reducer = createReducer(
  initialState,
  on(playlistAction.loadPlaylists, state => ({
    ...state, loading: true
  })),
  on(playlistAction.loadPlaylistsSuccess, (state, action) => ({
    ...state, loading: false, items: action.data
  })),
  on(playlistAction.loadPlaylistsFailure, (state, action) => ({
    ...state, loading: false, message: action.error?.message
  })),
  on(playlistAction.selectPlaylist, (state, { id }) => ({
    ...state, loading: false, selectedId: id
  })),
  on(playlistAction.editPlaylistMode, (state, { id }) => ({
    ...state, mode: 'edit', selectedId: id
  })),
  on(playlistAction.showPlaylistMode, (state, { id }) => ({
    ...state, mode: 'details', selectedId: id
  })),
  on(playlistAction.savePlaylist, (state) => ({
    ...state, loading: true
  })),
  on(playlistAction.savePlaylistSuccess, (state, action) => ({
    ...state, loading: false, items: state.items.map(
      p => p.id == action.data.id ? action.data : p)
  })),
  on(playlistAction.savePlaylistFailure, (state, action) => ({
    ...state, loading: false, message: action.error?.message
  })),
)


