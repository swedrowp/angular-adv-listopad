import { Component, OnInit } from '@angular/core';
import { AlbumSearchService } from '../../core/services/album-search/album-search.service';

@Component({
  selector: 'sgs-recent-search',
  templateUrl: './recent-search.component.html',
  styleUrls: ['./recent-search.component.scss']
})
export class RecentSearchComponent implements OnInit {

  constructor(
    private service: AlbumSearchService
  ) { }

  recent: string[] = []

  search(query: string) {
    this.service.searchAlbums(query)
  }

  ngOnInit(): void {
    this.service.recentSearchesChange.subscribe((query) => {
      this.recent.push(query);
      this.recent = this.recent.slice(Math.max(0, this.recent.length - 5));
    })
  }

}
