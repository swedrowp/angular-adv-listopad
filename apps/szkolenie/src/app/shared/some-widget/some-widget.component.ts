import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sgs-some-widget',
  templateUrl: './some-widget.component.html',
  styleUrls: ['./some-widget.component.scss']
})
export class SomeWidgetComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
