import { Component, EventEmitter } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout'

@Component({
  selector: 'sgs-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'MusicApp';
  isMobile = true;
  sidebarOpen = true

  constructor(
    private oauthService: OAuthService,
    breakpointObserver: BreakpointObserver) {

    breakpointObserver.observe([
      Breakpoints.HandsetLandscape,
      Breakpoints.HandsetPortrait
    ]).subscribe(result => this.isMobile = (result.matches))
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    // this.title = 'changed' //ExpressionChangedAfterItHasBeenCheckedError
    setTimeout(()=>{
      this.title = 'AppMusic'
    })

    // parentEmiter = new EventEmitter(true) // run with setTimeout
    // this.parentEmiter.emit(title)
  }

  login() {
    this.oauthService.initLoginFlowInPopup({}).then(() => {
      console.log(this.oauthService.getAccessToken())
    })
  }

  logout() {
    this.oauthService.logOut({})
  }

  ngDoCheck(): void {
    //Called every time that the input properties of a component or a directive are checked. Use it to extend change detection by performing a custom check.
    //Add 'implements DoCheck' to the class.
    // console.log('check')
  }
}
