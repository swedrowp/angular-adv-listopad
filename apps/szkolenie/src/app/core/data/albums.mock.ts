import { Album } from '../model/Album';

export const albumsMock: Album[] = [
  {
    id: 'Album 123',
    name: 'Album 123',
    type: 'album',
    images: [
      { height: 300, width: 300, url: 'https://www.placecage.com/c/200/200' }
    ]
  },
  {
    id: 'Album 345',
    name: 'Album 345',
    type: 'album',
    images: [
      { height: 300, width: 300, url: 'https://www.placecage.com/c/300/300' }
    ]
  },
  {
    id: 'Album 456',
    name: 'Album 456',
    type: 'album',
    images: [
      { height: 300, width: 300, url: 'https://www.placecage.com/c/400/400' }
    ]
  },
  {
    id: 'Album 456',
    name: 'Album 456',
    type: 'album',
    images: [
      { height: 300, width: 300, url: 'https://www.placecage.com/c/500/500' }
    ]
  },
  {
    id: 'Album 456',
    name: 'Album 456',
    type: 'album',
    images: [
      { height: 300, width: 300, url: 'https://www.placecage.com/c/600/600' }
    ]
  },
] as Album[]