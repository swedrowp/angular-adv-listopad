import { TestBed } from '@angular/core/testing';

import { AlbumSearchService } from './album-search.service';

describe('AlbumSearchService', () => {
  let service: AlbumSearchService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AlbumSearchService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
