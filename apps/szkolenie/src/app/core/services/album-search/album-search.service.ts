import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { albumsMock } from '../../data/albums.mock'
import { Album, AlbumsSearchResponse } from '../../model/Album';
import { debounceTime, map, startWith, tap } from 'rxjs/operators'
import { AsyncSubject, BehaviorSubject, concat, merge, of, ReplaySubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AlbumSearchService {
  private results = new BehaviorSubject<Album[]>(albumsMock)
  public resultsChanges = this.results.asObservable()

  private recentSearches = new ReplaySubject<string>(5)
  recentSearchesChange = this.recentSearches.asObservable()

  private query =  new BehaviorSubject<string>('batman')
  queryChanges = this.query.asObservable()
  message: any = ''

  constructor(private http: HttpClient) {
    // Store Query to recent queries
    this.query.subscribe(this.recentSearches)
    
    ;(window as any).subject = this.results
  }

  searchAlbums(query: string) {
    this.query.next(query)

    return this.fetchAlbumSearchResults(query)
    .subscribe({
      next: albums => this.results.next(albums),
      error: (error) => this.message = (error.message),
      complete: () => console.log('complete')
    })

  }
  
  fetchAlbumSearchResults(query: string) {
    return this.http.get<AlbumsSearchResponse>(//
      `https://api.spotify.com/v1/search`, {
      params: {
        type: 'album', q: query
      }
    })
    .pipe(map(resp => resp.albums.items))
  }

}
