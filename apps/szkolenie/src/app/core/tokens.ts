import { InjectionToken } from '@angular/core';

export const PLAYLISTS_MOCK = new InjectionToken('Mock Data for playlists service');
