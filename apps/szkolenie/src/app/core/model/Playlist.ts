import { ShowHideDirective } from '@angular/flex-layout';
import { Entity } from './Entity'
import { Track } from './Track'

export interface Playlist extends Entity {
  id: string;
  name: string;
  type: 'playlist'
  public: boolean;
  description: string;
  tracks?: Track[]
}
