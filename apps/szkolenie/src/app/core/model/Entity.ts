import { ExternalUrls } from './Utils';


export interface Entity {
  href: string;
  id: string;
  name: string;
  uri: string;
  external_urls: ExternalUrls;
}
